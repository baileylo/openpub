<!DOCTYPE html>
<html>
<head>
    <title>Place Autocomplete</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
        #description {
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
        }

        #infowindow-content .title {
            font-weight: bold;
        }

        #infowindow-content {
            display: none;
        }

        #map #infowindow-content {
            display: inline;
        }

        .pac-card {
            margin: 10px 10px 0 0;
            border-radius: 2px 0 0 2px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            outline: none;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
            background-color: #fff;
            font-family: Roboto;
        }

        #pac-container {
            padding-bottom: 12px;
            margin-right: 12px;
        }

        .pac-controls {
            display: inline-block;
            padding: 5px 11px;
        }

        .pac-controls label {
            font-family: Roboto;
            font-size: 13px;
            font-weight: 300;
        }

        #pac-input {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 400px;
        }

        #pac-input:focus {
            border-color: #4d90fe;
        }

        #title {
            color: #fff;
            background-color: #4d90fe;
            font-size: 25px;
            font-weight: 500;
            padding: 6px 12px;
        }
    </style>
</head>
<body>

<h1>My Stories &mdash; Create</h1>

<form action="{{ route('stories.edit', $story->id) }}" method="POST">
    {{ csrf_field() }}
    <label>
        Name
        <input type="text" name="name" placeholder="Disneyland Fall 2016" value="{{ old("name", $story->name) }}"/>
    </label>
    <label>
        Description:
        <textarea name="description">{{ old("description", $story->description) }}</textarea>
    </label>

    <label>
        Type: <select name="type">
            <option value="vacation" {{ old("type", $story->type) === 'vacation' ? 'selected' :'' }}>Vacation</option>
            <option value="trip" {{ old("type", $story->type) === 'trip' ? 'selected' :'' }}>Trip</option>
            <option value="hike" {{ old("type", $story->type) === 'hike' ? 'selected' :'' }}>Hike</option>
            <option value="camping" {{ old("type", $story->type) === 'camping' ? 'selected' :'' }}>Camping</option>
            <option value="cruise" {{ old("type", $story->type) === 'cruise' ? 'selected' :'' }}>Cruise</option>
        </select>
    </label>

    <label>
        Start Date of Story:
        <input type="date" name="happened_at" value="{{ old("happened_at", $story->happened_at->format('Y-m-d')) }}">
    </label>

    <label>
        Location:
        <input type="text" name="location[name]" id="address-look-up" placeholder="Enter Location" value="{{ $story->location->name ?? '' }}">
        <input type="hidden" name="location[place_id]" id="address-place-id" value="{{ $story->location->place_id ?? '' }}">
        <input type="hidden" name="location[latitude]" id="address-latitude" value="{{ $story->location->latitude ?? '' }}">
        <input type="hidden" name="location[longitude]" id="address-longitude" value="{{ $story->location->longitude ?? '' }}">
    </label>

    <input type="submit" value="Update Story"/>
    <a href="{{ route('stories') }}">Cancel</a>
</form>

<script>
    function initLookUp() {
        var input = document.getElementById('address-look-up');
        var autocomplete = new google.maps.places.Autocomplete(input);

        autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            document.getElementById("address-place-id").value =  place.place_id;
            document.getElementById("address-latitude").value =  place.geometry.location.lat();
            document.getElementById("address-longitude").value =  place.geometry.location.lng();
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ $api_key }}&libraries=places&callback=initLookUp"
        async defer></script>
</body>
</html>