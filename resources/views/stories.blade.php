<h1>My Stories</h1>


<table>
    <tr>
        <th>Name</th>
        <th>Type</th>
        <th>Description</th>
        <th>Date</th>
        <th>Operations</th>
    </tr>
    @foreach ($stories as $story)
    <tr>
        <td>{{ $story->name }}</td>
        <td>{{ $story->type }}</td>
        <td>{{ $story->description }}</td>
        <td>{{ $story->happened_at->format('F jS, Y') }}</td>
        <td>
            <form action="{{ route('stories.delete', $story) }}" method="POST">
                {{ method_field('delete') }}
                {{ csrf_field() }}
                <input type="submit" value="Delete">
            </form>
            |
            <a href="{{ route('stories.edit', $story) }}">Edit</a>
        </td>
    </tr>
    @endforeach
</table>

<div>
    <a href="{{ route('stories.create') }}">Add New Story</a>
</div>