<h1>My Stories &mdash; Create</h1>

<form action="{{ route('stories.create') }}" method="POST">
    {{ csrf_field() }}
    <label>
        Name
        <input type="text" name="name" placeholder="Disneyland Fall 2016" value="{{ old("name") }}"/>
    </label>
    <label>
        Description:
        <textarea name="description" value="{{ old("description") }}"></textarea>
    </label>

    <label>
        Type: <select name="type">
            <option value="vacation" {{ old("type") === 'vacation' ? 'selected' :'' }}>Vacation</option>
            <option value="trip" {{ old("type") === 'trip' ? 'selected' :'' }}>Trip</option>
            <option value="hike" {{ old("type") === 'hike' ? 'selected' :'' }}>Hike</option>
            <option value="camping" {{ old("type") === 'camping' ? 'selected' :'' }}>Camping</option>
            <option value="cruise" {{ old("type") === 'cruise' ? 'selected' :'' }}>Cruise</option>
        </select>
    </label>

    <label>
        Start Date of Story:
        <input type="date" name="happened_at" value="{{ old("happened_at") }}">
    </label>

    <label>
        Location:
        <input type="text" name="location[name]" id="address-look-up" placeholder="Enter Location">
        <input type="hidden" name="location[place_id]" id="address-place-id" value="">
        <input type="hidden" name="location[latitude]" id="address-latitude" value="">
        <input type="hidden" name="location[longitude]" id="address-longitude" value="">
    </label>

    <input type="submit" value="Save Story"/>
    <a href="{{ route('stories') }}">Cancel</a>
</form>

<script>
    function initLookUp() {
        var input = document.getElementById('address-look-up');
        var autocomplete = new google.maps.places.Autocomplete(input);

        autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            document.getElementById("address-place-id").value =  place.place_id;
            document.getElementById("address-latitude").value =  place.geometry.location.lat();
            document.getElementById("address-longitude").value =  place.geometry.location.lng();
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ $api_key }}&libraries=places&callback=initLookUp"
        async defer></script>
