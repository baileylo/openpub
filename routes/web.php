<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/** @var $router \Illuminate\Routing\Router */

Route::get('/', function () {
    return view('welcome');
});

$router->get('stories')
    ->name('stories')
    ->uses('StoryController@index');

$router->get('stories/create')
    ->name('stories.create')
    ->uses('StoryController@create');

$router->post('stories/create')
    ->uses('StoryController@save');

$router->get('stories/{story}/edit')
    ->name('stories.edit')
    ->uses('StoryController@edit');

$router->post('stories/{story}/edit')
    ->name('stories.edit')
    ->uses('StoryController@update');


$router->delete('/stories/{story}/delete')
    ->name('stories.delete')
    ->uses('StoryController@delete');
