<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Story
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $type
 * @property \Carbon\Carbon $happened_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int|null $location_id
 * @property-read \App\Location|null $location
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Story whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Story whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Story whereHappenedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Story whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Story whereLocationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Story whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Story whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Story whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Story extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'type', 'happened_at', 'location'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['happened_at'];

    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    public function setLocationAttribute($location)
    {
        $this->attributes['location_id'] = $location->id;
        $this->relations['location'] = $location;
    }
}
