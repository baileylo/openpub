<?php

namespace App\Http\Controllers;

use App\Location;
use App\Story;
use Illuminate\Http\Request;
use Illuminate\Routing\ResponseFactory;

class StoryController extends Controller
{
    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    /**
     * @var string
     */
    private $google_api_key;

    public function __construct(ResponseFactory $responseFactory, string $google_api_key)
    {
        $this->responseFactory = $responseFactory;
        $this->google_api_key = $google_api_key;
    }

    /**
     * Shows a sortable list of Stories
     */
    public function index()
    {
        return $this->responseFactory->view('stories', [
            'stories' => Story::paginate()
        ]);
    }

    public function create()
    {
        return $this->responseFactory->view('stories-create', ['api_key' => $this->google_api_key]);
    }

    public function save()
    {
        $this->validate(request(), [
            'name'               => 'required|string|max:255|min:5',
            'type'               => 'required',
            'description'        => 'required',
            'happened_at'        => 'required|date',
            'location.name'      => 'nullable|string',
            'location.place_id'  => 'nullable',
            'location.latitude'  => 'nullable|numeric',
            'location.longitude' => 'nullable|numeric'
        ]);

        /** @var Story $story */
        $data             = request()->only('name', 'type', 'description', 'happened_at');
        $data['location'] = Location::firstOrCreate(request('location'));

        Story::create($data);

        return $this->responseFactory->redirectTo('stories');
    }

    public function edit(Story $story)
    {
        return $this->responseFactory->view('stories-edit', ['story' => $story, 'api_key' => $this->google_api_key]);
    }

    public function update(Story $story)
    {
        $this->validate(request(), [
            'name'               => 'required|string|max:255|min:5',
            'type'               => 'required',
            'description'        => 'required',
            'happened_at'        => 'required|date',
            'location.place_id'  => 'nullable',
            'location.latitude'  => 'nullable|numeric',
            'location.longitude' => 'nullable|numeric'
        ]);

        $data = request()->only('name', 'type', 'description', 'happened_at');
        $data['location'] = Location::firstOrCreate(request('location'));

        $story->update($data);

        return $this->responseFactory->redirectTo('stories');
    }


    public function delete(Story $story)
    {
        $story->delete();

        return $this->responseFactory->redirectTo('stories');
    }
}
