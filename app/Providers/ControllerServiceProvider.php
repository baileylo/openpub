<?php

namespace App\Providers;


use App\Http\Controllers\StoryController;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Support\ServiceProvider;

class ControllerServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(StoryController::class, function () {
            return new StoryController(
                $this->app[ResponseFactory::class],
                $this->app['config']['services.google.places']
            );
        });
    }
}